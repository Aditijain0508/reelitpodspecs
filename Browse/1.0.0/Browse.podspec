Pod::Spec.new do |s|
  s.name         = "Browse"
  s.version      = "3.0.0"
  s.summary      = "Description of your project"
  s.description  = "Mandatorily longer description of your project"
  s.homepage     = "https://github.com/YourUserName/NameOfYourProject"
  s.platform     = :ios, "15.0"
  s.license      = "Description of your licence, name or otherwise"
  s.author       = { "Your name occupation" => "your@email.com" }
  s.source       = { :git => 'https://gitlab.com/Aditijain0508/reelitbrowse.git', :tag => "3.0.0" }
  s.source_files =  "Browse/Classes/**/*.swift" # path to your classes. You can drag them into their own folder.
  s.dependency   'Core'
  s.dependency    'Network'
  s.requires_arc = true
  s.swift_version= '5.0'
  s.xcconfig     = { 'SWIFT_VERSION' => '4.0' }
  s.default_subspecs = 'Framework'
  s.subspec 'Framework' do |evernote|
    evernote.source_files = 'Browse/Classes/**/*.{h,m,swift,storyboard}'
  end

end


