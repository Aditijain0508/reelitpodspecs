Pod::Spec.new do |s|
  s.name         = "Login"
  s.version      = "3.0.0"
  s.summary      = "Description of your project"
  s.description  = "Mandatorily longer description of your project"
  s.homepage     = "https://github.com/YourUserName/NameOfYourProject"
  s.platform     = :ios, "15.0"
  s.license      = "Description of your licence, name or otherwise"
  s.author       = { "Your name occupation" => "your@email.com" }
  s.source       = { :git => 'https://gitlab.com/Aditijain0508/reelitlogin.git', :tag => "1.0.0"}
  s.source_files =  "Login/Classes/**/*.swift" # path to your classes. You can drag them into their own folder.
  s.dependency   'Core'
  s.dependency    'Network'
  s.dependency 'PromiseKit'
  s.dependency 'FirebaseAuth'
  s.requires_arc = true
  s.default_subspecs = 'Framework'
  s.resources = 'Login/Classes/**/*.{xcassets,json,png}'
  s.subspec 'Framework' do |login|
    login.source_files = 'Login/Classes/**/*.{h,m,swift,storyboard,xcassets}'
    
  end

end


