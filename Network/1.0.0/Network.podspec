Pod::Spec.new do |s|
  s.name         = "Network"
  s.version      = "1.0.0"
  s.summary      = "Description of your project"
  s.description  = "Mandatorily longer description of your project"
  s.homepage     = "https://github.com/YourUserName/NameOfYourProject"
  s.license      = "Description of your licence, name or otherwise"
  s.author       = { "Your name occupation" => "your@email.com" }
  s.platform     = :ios, "15.0"
  s.source       = { :git => 'https://gitlab.com/Aditijain0508/reelitnetwork.git', :tag => "1.0.0" }
  s.source_files =  "Network/Classes/**/*.swift" # path to your classes. You can drag them into their own folder.
  
  s.requires_arc = true
  s.swift_version= '4.0'
  s.xcconfig     = { 'SWIFT_VERSION' => '5.0' }
  s.default_subspecs = 'Framework'

  s.subspec 'Framework' do |evernote|
    evernote.source_files = 'Network/Classes/**/*.{h,m,swift}'
  end

end


